# MyProfile app 

SEPARATOR = '------------------------------------------'


class App:
    def __init__(self):
        # user profile
        self.name = ''
        self.age = 0
        self.phone = ''
        self.email = ''
        self.info = ''
        # social links
        self.vkontakte = ''
        self.facebook = ''
        self.instagram = ''
        self.telegram = ''
        self.tiktok = ''

    def run(self):
        print('Приложение Profile')
        print('Сохраняй информацию о себе и выводи ее в разных форматах')

        while True:
            # main menu
            print(SEPARATOR)
            print('ГЛАВНОЕ МЕНЮ')
            print('1 - Ввести или обновить информацию')
            print('2 - Вывести информацию')
            print('0 - Завершить работу')

            option = int(input('Введите номер пункта меню: '))
            if option == 0:
                break

            if option == 1:
                # submenu 1: edit info
                self.edit_info_menu()
            elif option == 2:
                # menu 2: print info
                self.print_info_menu()
            else:
                print('Введите корректный пункт меню')

    def edit_info_menu(self):
        while True:
            print(SEPARATOR)
            print('ВВЕСТИ ИЛИ ОБНОВИТЬ ИНФОРМАЦИЮ')
            print('1 - Общая информация')
            print('2 - Социальные сети и мессенджеры')
            print('0 - Назад')

            option = int(input('Введите номер пункта меню: '))
            if option == 0:
                break
            if option == 1:
                # input general info
                self.name = input('Введите имя: ')
                while True:
                    # validate user age
                    self.age = int(input('Введите возраст: '))
                    if self.age > 0:
                        break
                    print('Возраст должен быть положительным')

                user_phone = input('Введите номер телефона (+7ХХХХХХХХХХ): ')
                self.phone = ''
                for ch in user_phone:
                    if ch == '+' or ('0' <= ch <= '9'):
                        self.phone += ch

                self.email = input('Введите адрес электронной почты: ')
                self.info = input('Введите дополнительную информацию:\n')
            elif option == 2:
                # input social links
                self.vkontakte = input('Введите адрес профиля Вконтакте: ')
                self.facebook = input('Введите адрес профиля Facebook: ')
                self.instagram = input('Введите адрес профиля Instagram: ')
                self.telegram = input('Введите логин Telegram: ')
                self.tiktok = input('Введите логин Tiktok: ')
            else:
                print('Введите корректный пункт меню')

    def print_info_menu(self):
        while True:
            print(SEPARATOR)
            print('ВЫВЕСТИ ИНФОРМАЦИЮ')
            print('1 - Общая информация')
            print('2 - Вся информация')
            print('0 - Назад')

            option = int(input('Введите номер пункта меню: '))
            if option == 0:
                break

            print(SEPARATOR)
            if option == 1:
                # print general information
                self.print_general_info()
            elif option == 2:
                # print full information
                self.print_full_info()
            else:
                print('Введите корректный пункт меню')

    def print_general_info(self):
        print('Имя:    ', self.name)
        if 11 <= self.age % 100 <= 19:
            years_name = 'лет'
        elif self.age % 10 == 1:
            years_name = 'год'
        elif 2 <= self.age % 10 <= 4:
            years_name = 'года'
        else:
            years_name = 'лет'
        print('Возраст:', self.age, years_name)
        print('Телефон:', self.phone)
        print('E-mail: ', self.email)
        if self.info:
            print('')
            print('Дополнительная информация:')
            print(self.info)

    def print_full_info(self):
        self.print_general_info()
        # print social links
        print('')
        print('Социальные сети и мессенджеры')
        print('Вконтакте:', self.vkontakte)
        print('Facebook: ', self.facebook)
        print('Instagram:', self.instagram)
        print('Telegram: ', self.telegram)
        print('Tiktok:   ', self.tiktok)


my_app = App()
my_app.run()
